import * as THREE from "three";
import { PCDLoader } from "three/addons/loaders/PCDLoader.js";
import { OrbitControls } from "three/addons/controls/OrbitControls";
// import { OBJLoader } from 'https://threejs.org/examples/jsm/loaders/OBJLoader.js';

const scene = new THREE.Scene();
const w = 1000; /* window.innerWidth */
const h = 800; /* window.innerHeight */
const camera = new THREE.PerspectiveCamera(75, w / h, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();

const controls = new OrbitControls(camera, renderer.domElement);

renderer.setSize(w, h, false);
camera.position.x = 0;
camera.position.y = 0;
camera.position.z = 20;

document.getElementById("pointCloudContainer").appendChild(renderer.domElement);

const animate = function () {
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
};

animate();
renderer.render(scene, camera);

function loadPCD(file) {
  const loader = new PCDLoader();

  console.log(file.files[0]);

  if (file.files.length) {
    var reader = new FileReader();

    reader.onload = function (e) {
      var points = loader.parse(e.target.result);
      console.log(points);
      points.material.size = 0.025;
      points.geometry.attributes.color.array =
        points.geometry.attributes.color.array.map((p) => {
          return 1.0;
        });
      scene.add(points);
      renderer.render(scene, camera);
    };

    reader.readAsArrayBuffer(file.files[0]);
  }
}

function uploadFile() {
  const form = $("#uploadForm")[0];
  var formData = new FormData(form);

  loadPCD(form[0]);
}

window.uploadFile = uploadFile;
