# Lidar Object Detection

## Lidap perception papers

## Overview:
- https://arxiv.org/pdf/2004.08467.pdf
- https://www.sciencedirect.com/science/article/pii/S0097849321001321
- https://ieeexplore.ieee.org/abstract/document/9181591

## Lidar Object Detection models:
- https://openaccess.thecvf.com/content_cvpr_2018/html/Yang_PIXOR_Real-Time_3D_CVPR_2018_paper.html
- https://openaccess.thecvf.com/content_cvpr_2017/papers/Chen_Multi-View_3D_Object_CVPR_2017_paper.pdf
- https://arxiv.org/abs/1812.05784

## Environment 

Download dataset:
https://www.nuscenes.org/

Run docker:
- `docker-compose -f ./docker-compose.yml build`
- `docker-compose -f ./docker-compose.yml run --rm shell`

Run jupyter inside docker:
- `jupyter notebook --ip 0.0.0.0 --no-browser --allow-root`

Run service:
- `hypercorn app.main:app`

## ROS bag

Для преобразования сцен из NuScenes в rosbag для тестирования и инференса воспользоваться утилитой [nuscenes2bag](https://github.com/clynamen/nuscenes2bag).

![Scene 61](/images/bag_video.gif)

