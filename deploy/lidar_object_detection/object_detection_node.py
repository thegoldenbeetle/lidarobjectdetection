import rclpy
from rclpy.node import Node
from sensor_msgs.msg import PointCloud2
from vision_msgs.msg import Detection3D


class ObjectDetectionNode(Node):
    def __init__(self):
        super().__init__("object_detection_node")
        self.subscription = self.create_subscription(
            PointCloud2, "lidar_pointcloud", self.lidar_callback, 10
        )
        self.publisher = self.create_publisher(
            Detection3D, "object_detections", 10
        )

        # TODO:
        # load model from checkpoints
        # move to gpu

    def lidar_callback(self, msg):
        detected_objects = self.perform_detection(msg)

        detection_msg = Detection3D()
        detection_msg.header = msg.header
        detection_msg.detections = detected_objects
        self.publisher.publish(detection_msg)

    def perform_detection(self, pointcloud_msg):
        # TODO: process input pointcloud by model

        # 1. Pointcloud to input representation
        # 2. Move input batch to gpu
        # 3. Process by model
        # 4. Convert model outputs to Detection3D interface
        return []


def main(args=None):
    rclpy.init(args=args)
    node = ObjectDetectionNode()
    rclpy.spin(node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
