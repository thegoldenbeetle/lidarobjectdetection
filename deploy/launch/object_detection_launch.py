from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription(
        [
            Node(
                package="lidar_object_detection",
                executable="object_detection_node",
                name="object_detection_node",
                output="screen",
                emulate_tty=True,
                parameters=[
                    {"lidar_topic": "input_pointcloud"},
                    {"detection_topic": "object_detections"},
                ],
                arguments=[
                    "--lidar-topic",
                    "input_pointcloud",
                    "--detection-topic",
                    "object_detections",
                ],
            ),
        ]
    )
