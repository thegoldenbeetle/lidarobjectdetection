Build and run package:
- `colcon build --packages-select lidar_object_detection`
- `ros2 launch lidar_object_detection object_detection_launch.py lidar_topic:=input_pointcloud detection_topic:=object_detections`