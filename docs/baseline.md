# Baseline model

Baseline модель представляет собой модель, основанную на архитектуре PIXOR.
Точные параметры модели и эксперимента можно посмотреть в коде, соответствует коммиту
[8392064bb1f4d02642c99629b0bd18b0221c80f2](https://gitlab.com/thegoldenbeetle/lidarobjectdetection/-/commits/8d668d675e218cb8c7b9cdcbc794c0048158d1ee)

Графики процесса обучения и валидации модели:

оранжевый цвет - обучение, синий цвет - валидация

| Loss Total                          | Loss Classes                                 | Loss Boxes                               |
| ----------------------------------- | -------------------------------------------- | ---------------------------------------- |
| ![](./imgs_baseline/loss_total.png) | ![](./imgs_baseline/loss_classification.png) | ![](./imgs_baseline/loss_regression.png) |

| F1 score classes                          | Mean average precision                 | Mean average recall                 |
| ----------------------------------------- | -------------------------------------- | ----------------------------------- |
| ![](./imgs_baseline/f1_score_classes.png) | ![](./imgs_baseline/avg_precision.png) | ![](./imgs_baseline/avg_recall.png) |

