# Learning rate scheduler experiments

Для повышения качества модели были проведены эксперименты с различными патернами изменения learning rate. Наилучший результат был получен при использовании [OneCycleLR](https://pytorch.org/docs/stable/generated/torch.optim.lr_scheduler.OneCycleLR.html). Начальный рост `lr` идёт по линейной стратегии (`anneal_strategy="linear"`), а после по косинусной стратегии (`anneal_strategy="cos"`). Значения `lr` на протяжении обучения представлены на графике ниже.

Был  осуществлен перебор различными правилами изменения lr, метрики лучшего варианта в сравнении с baseline и экспериментом с аугментациями приведены в таблице ниже:

|                    | F1 score | Mean average precision | Mean average recall |
| ------------------ | -------- | ---------------------- | ------------------- |
| baseline           | 0.75     | 0.82                   | 0.69                |
| augmentation model | 0.79     | 0.83                   | 0.76                |
| best model         | 0.81     | 0.85                   | 0.78                |

Точные параметры модели и эксперимента можно посмотреть в коде, соответствует коммиту
[51d219c5cef5e675989d969e2067203cdfd557e8](https://gitlab.com/thegoldenbeetle/lidarobjectdetection/-/commits/51d219c5cef5e675989d969e2067203cdfd557e8)

Графики прогресса обучения и валидации для лучшей по качеству модели:

| Loss Total                              | Loss Classes                              | Loss Boxes                              |
| --------------------------------------- | ----------------------------------------- | --------------------------------------- |
| ![](./imgs_lr_scheduler/loss_total.png) | ![](./imgs_lr_scheduler/loss_classes.png) | ![](./imgs_lr_scheduler/loss_boxes.png) |

| F1 score classes                              | Mean average precision                     | Mean average recall                     |
| --------------------------------------------- | ------------------------------------------ | --------------------------------------- |
| ![](./imgs_lr_scheduler/f1_score_classes.png) | ![](./imgs_lr_scheduler/avg_precision.png) | ![](./imgs_lr_scheduler/avg_recall.png) |
| Learning rate                                 |                                            |                                         |
| ![](./imgs_lr_scheduler/lr.png)               |                                            |                                         |

