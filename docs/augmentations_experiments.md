# Augmentation experiments

Для повышения качества детекций были реализованы различные аугментации:

Point cloud аугментации:

- shift - сдвиг
- scale - изменение масштаба
- rotate - поворот относительно оси z
- dropout_probability - вероятность выбросить точку

Objects аугментации:

- z_axis_rotation - поворот бокса относительно оси z

Был  осуществлен перебор различных параметров данных аугментаций, метрики лучшего варианта в сравнении с baseline приведены в таблице ниже:

|            | F1 score | Mean average precision | Mean average recall |
| ---------- | -------- | ---------------------- | ------------------- |
| baseline   | 0.75     | 0.82                   | 0.69                |
| best model | 0.79     | 0.83                   | 0.76                |

Точные параметры модели и эксперимента можно посмотреть в коде, соответствует коммиту
[6732e3297d7e284417401eeafd7b1c91ab2e5205](https://gitlab.com/thegoldenbeetle/lidarobjectdetection/-/commits/6732e3297d7e284417401eeafd7b1c91ab2e5205)

Графики просесса обучения и валидации для лучшей по качеству модели:

| Loss Total                               | Loss Classes                               | Loss Boxes                               |
| ---------------------------------------- | ------------------------------------------ | ---------------------------------------- |
| ![](./imgs_augmentations/loss_total.png) | ![](./imgs_augmentations/loss_classes.png) | ![](./imgs_augmentations/loss_boxes.png) |

| F1 score classes                               | Mean average precision                      | Mean average recall                      |
| ---------------------------------------------- | ------------------------------------------- | ---------------------------------------- |
| ![](./imgs_augmentations/f1_score_classes.png) | ![](./imgs_augmentations/avg_precision.png) | ![](./imgs_augmentations/avg_recall.png) |

