from dataclasses import dataclass
from math import radians
from typing import Tuple


@dataclass
class ObjectAugmentationSettings:
    probability: float = 0.2
    z_axis_rotation: float = radians(10)  # noqa: RUF009


@dataclass
class CloudAugmentationSettings:
    probability: float = 0.5
    shift: Tuple[float] = (3.0, 3.0, 0.3)
    scale: float = 0.08
    rotate: float = radians(15)  # noqa: RUF009
    dropout_probability: float = 0.01
