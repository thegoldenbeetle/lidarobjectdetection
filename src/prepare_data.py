import pandas as pd
from nuscenes.nuscenes import NuScenes

train_scenes = [
    "cc8c0bf57f984915a77078b10eb33198",
    "6f83169d067343658251f72e1dd17dbc",
    "2fc3753772e241f2ab2cd16a784cc680",
    "c5224b9b454b4ded9b5d2d2634bbda8a",
    "325cef682f064c55a255f2625c533b75",
    "d25718445d89453381c659b9c8734939",
    "de7d80a1f5fb4c3e82ce8a4f213b450a",
]

val_scenes = [
    "fcbccedd61424f1b85dcbf8f897f9754",
    "e233467e827140efa4b42d2b4c435855",
    "bebf5f5b2a674631ab5c88fd1aa9e87a",
]


def save_samples_tokens_to_file(file_path, scenes_tokens, nusc):
    # pylint: disable=W0621
    samples_tokens = []
    for scene_token in scenes_tokens:
        scene = nusc.get("scene", scene_token)
        current_sample_token = scene["first_sample_token"]
        while current_sample_token != "":
            current_sample = nusc.get("sample", current_sample_token)
            samples_tokens.append(current_sample["token"])
            current_sample_token = current_sample["next"]

    df = pd.DataFrame({"tokens": samples_tokens})
    df.to_csv(file_path, index=False, header=False)


if __name__ == "__main__":
    DATA_ROOT = "/data/v1.0-mini/"
    nusc = NuScenes(version="v1.0-mini", dataroot=DATA_ROOT, verbose=True)

    # pylint: disable=C0103
    train_samples_token_file_path = "./data/train_samples.csv"
    val_samples_token_file_path = "./data/val_samples.csv"

    save_samples_tokens_to_file(
        train_samples_token_file_path, train_scenes, nusc
    )
    save_samples_tokens_to_file(val_samples_token_file_path, val_scenes, nusc)
