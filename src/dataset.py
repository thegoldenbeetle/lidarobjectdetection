import math
import os
import random
from pathlib import Path

import lightning.pytorch as pl
import matplotlib.pyplot as plt
import numpy as np
import torch
from matplotlib.patches import Rectangle
from nuscenes.nuscenes import NuScenes
from nuscenes.utils.data_classes import LidarPointCloud
from nuscenes.utils.splits import create_splits_scenes
from scipy.spatial.transform import Rotation
from torch.utils.data import DataLoader, Dataset, Subset, default_collate
from torchvision.io import read_image

from augmentations import CloudAugmentationSettings, ObjectAugmentationSettings
from src import params
from src.categories import (
    category_id_to_color,
    category_to_id,
    nuscenes_category_to_our,
)
from src.point_clout_utils import (
    get_density_map,
    get_height_map,
    get_intensity_map,
    projection_front_view,
)


class BevRepresentation:
    def __init__(self, pc_points_numpy):
        grid_mask = self.get_grid_mask(pc_points_numpy)
        self.pc_points_in_grid_numpy = pc_points_numpy[grid_mask]

        x_indices = (
            (self.pc_points_in_grid_numpy[:, 0] - params.X_RANGE[0])
            / params.CELL_SIZE
        ).astype(int)
        y_indices = (
            (self.pc_points_in_grid_numpy[:, 1] - params.Y_RANGE[0])
            / params.CELL_SIZE
        ).astype(int)
        z_indices = (
            (self.pc_points_in_grid_numpy[:, 2] - params.Z_RANGE[0])
            / math.ceil((params.Z_RANGE[1] - params.Z_RANGE[0]) / params.M)
        ).astype(int)
        mask = (
            (z_indices < params.M)
            & (z_indices >= 0)
            & (x_indices < params.GRID_SIZE_X)
            & (x_indices >= 0)
            & (y_indices < params.GRID_SIZE_Y)
            & (y_indices >= 0)
        )
        x_indices = x_indices[mask]
        y_indices = y_indices[mask]
        z_indices = z_indices[mask]
        self.pc_points_in_grid_numpy = self.pc_points_in_grid_numpy[mask]

        density_map = get_density_map(x_indices, y_indices)
        self.density_map = np.expand_dims(density_map, axis=2)

        intencity_map = get_intensity_map(
            self.pc_points_in_grid_numpy, x_indices, y_indices
        )
        self.intencity_map = np.expand_dims(intencity_map, axis=2)

        self.height_map = get_height_map(
            self.pc_points_in_grid_numpy, x_indices, y_indices, z_indices
        )

    def get_grid_mask(self, pc_points_numpy):
        return (
            (pc_points_numpy[:, 0] >= params.X_RANGE[0])
            & (pc_points_numpy[:, 0] < params.X_RANGE[1])
            & (pc_points_numpy[:, 1] >= params.Y_RANGE[0])
            & (pc_points_numpy[:, 1] < params.Y_RANGE[1])
            & (pc_points_numpy[:, 2] >= params.Z_RANGE[0])
            & (pc_points_numpy[:, 2] < params.Z_RANGE[1])
        )

    def get_bev_representation(self):
        return np.concatenate(
            (self.density_map, self.intencity_map, self.height_map), axis=2
        )


class FovRepresentation:
    # pylint: disable=R0903

    def __init__(self, pc_points_numpy):
        self.height_fov = projection_front_view(
            pc_points_numpy,
            params.VRES,
            params.HRES,
            params.VFOV,
            val="height",
            y_fudge=params.Y_FUDGE,
        )
        self.depth_fov = projection_front_view(
            pc_points_numpy,
            params.VRES,
            params.HRES,
            params.VFOV,
            val="depth",
            y_fudge=params.Y_FUDGE,
        )
        self.reflectance_fov = projection_front_view(
            pc_points_numpy,
            params.VRES,
            params.HRES,
            params.VFOV,
            val="reflectance",
            y_fudge=params.Y_FUDGE,
        )

    def get_fov_representation(self):
        return np.stack(
            (self.height_fov, self.depth_fov, self.reflectance_fov)
        )


class LidarNuScenesItem:
    def __init__(self, sample, nusc, split):
        self.sample = sample
        self.nusc = nusc

        self.camera_image_tensor = self.get_camera_image_tensor()
        self.pc_points_numpy = self.get_lidar_pointcloud()

        self.bev_representation = BevRepresentation(self.pc_points_numpy)
        self.fov_representation = FovRepresentation(self.pc_points_numpy)

        annotations = self.get_annotations()
        inside_grid = self.bev_representation.get_grid_mask(annotations)
        self.annotations = annotations[inside_grid]

        if "train" in split:
            self.annotations = self.apply_object_augmentation(self.annotations)
            self.pc_points_numpy = self.apply_cloud_augmentation()

    def apply_object_augmentation(self, annotations):
        aug_settings = ObjectAugmentationSettings()
        if random.random() < aug_settings.probability:
            for _, annotation in enumerate(annotations):
                rotation = Rotation.from_euler(
                    "z",
                    random.uniform(
                        -aug_settings.z_axis_rotation,
                        aug_settings.z_axis_rotation,
                    ),
                )
                annotation[6] = rotation.apply(annotation[6:7])

        return annotations

    def apply_cloud_augmentation(self):
        aug_settings = CloudAugmentationSettings()
        if random.random() < aug_settings.probability:
            shift = [
                random.uniform(-aug_settings.shift[0], aug_settings.shift[0]),
                random.uniform(-aug_settings.shift[1], aug_settings.shift[1]),
                random.uniform(-aug_settings.shift[2], aug_settings.shift[2]),
            ]
            self.pc_points_numpy += np.array(shift)

            scale_factor = 1.0 + random.uniform(
                -aug_settings.scale, aug_settings.scale
            )
            self.pc_points_numpy *= scale_factor

            rotation_angle = random.uniform(
                -aug_settings.rotate, aug_settings.rotate
            )
            rotation_matrix = Rotation.from_euler(
                "z", rotation_angle
            ).as_matrix()
            self.pc_points_numpy = np.dot(
                self.pc_points_numpy, rotation_matrix.T
            )

            dropout_mask = (
                np.random.rand(*self.pc_points_numpy.shape)
                < aug_settings.dropout_probability
            )
            self.pc_points_numpy[dropout_mask] = 0.0

        return self.pc_points_numpy

    def to_list_to_train(self):
        bev_tensor = torch.from_numpy(
            self.bev_representation.get_bev_representation()
        )
        bev_tensor = bev_tensor.permute(2, 0, 1).float()
        return [
            self.camera_image_tensor,
            bev_tensor,
            torch.from_numpy(self.fov_representation.get_fov_representation()),
            torch.from_numpy(self.annotations),
        ]

    def get_camera_image_tensor(self):
        camera_data = self.nusc.get(
            "sample_data", self.sample["data"]["CAM_FRONT"]
        )
        camera_image_path = os.path.join(
            self.nusc.dataroot, camera_data["filename"]
        )
        return read_image(str(camera_image_path))

    def get_lidar_pointcloud(self):
        lidar_data = self.nusc.get(
            "sample_data", self.sample["data"]["LIDAR_TOP"]
        )
        lidar_pointcloud_path = os.path.join(
            self.nusc.dataroot, lidar_data["filename"]
        )
        lidar_pointcloud = LidarPointCloud.from_file(lidar_pointcloud_path)
        return lidar_pointcloud.points.T

    def get_annotations(self):
        lidar_data_token = self.sample["data"]["LIDAR_TOP"]
        sample_data = self.nusc.get_sample_data(lidar_data_token)
        annotations = []
        for box in sample_data[1]:
            yaw, _, _ = box.orientation.yaw_pitch_roll
            category_id = category_to_id[nuscenes_category_to_our[box.name]]
            relative_center = (
                box.center
                - [params.X_RANGE[0], params.Y_RANGE[0], params.Z_RANGE[0]]
            ) / [
                (params.X_RANGE[1] - params.X_RANGE[0]),
                (params.Y_RANGE[1] - params.Y_RANGE[0]),
                (params.Z_RANGE[1] - params.Z_RANGE[0]),
            ]
            annotations.append(
                (*box.center, *box.wlh, yaw, category_id, *relative_center)
            )
        return np.array(annotations)

    def render_sample(self):
        self.nusc.render_sample(self.sample["token"])

    def show_bev(self, annotations=None):
        x = self.bev_representation.pc_points_in_grid_numpy[:, 0]
        y = self.bev_representation.pc_points_in_grid_numpy[:, 1]
        plt.scatter(
            y,
            x,
            c=self.bev_representation.pc_points_in_grid_numpy[:, 3],
            s=0.2,
        )
        if annotations is not None:
            for annotation in annotations:
                x, y, _, w, l, _, yaw, cat_id = annotation
                plt.gca().add_patch(
                    Rectangle(
                        (y - l / 2, x - w / 2),
                        l,
                        w,
                        angle=-yaw / np.pi * 360,
                        edgecolor=category_id_to_color[cat_id],
                        facecolor="none",
                        rotation_point="center",
                        lw=1,
                    )
                )
        plt.show()
        plt.imshow(self.bev_representation.density_map, cmap="gray")
        plt.show()
        plt.imshow(self.bev_representation.intencity_map, cmap="gray")
        plt.show()
        plt.imshow(self.bev_representation.height_map[:, :, 1], cmap="gray")
        plt.show()

    def show_fov(self):
        plt.imshow(self.fov_representation.height_fov, cmap="jet")
        plt.show()
        plt.imshow(self.fov_representation.depth_fov, cmap="jet")
        plt.show()
        plt.imshow(self.fov_representation.reflectance_fov, cmap="jet")
        plt.show()


def lidar_collate(data):
    img = []
    bev = []
    fev = []
    annotations = []
    for i, b, f, a in (x.to_list_to_train() for x in data):
        img.append(i)
        bev.append(b)
        fev.append(f)
        annotations.append(a)
    return (
        default_collate(img),
        default_collate(bev),
        default_collate(fev),
        annotations,
    )


class LidarNuScenesDataset(Dataset):
    """Lidar object detection dataset."""

    def __init__(
        self,
        dataset_root_dir="/data/v1.0-mini",
        dataset_version="v1.0-mini",
        split: str = "mini_train",
    ):
        self.nusc = NuScenes(
            version=dataset_version, dataroot=dataset_root_dir, verbose=True
        )
        split_scenes = set(create_splits_scenes()[split])
        self.samples = []
        for scene in self.nusc.scene:
            if scene["name"] not in split_scenes:
                continue
            token = scene["first_sample_token"]
            while token != "":
                sample = self.nusc.get("sample", token)
                exist = os.path.exists(
                    os.path.join(
                        dataset_root_dir,
                        self.nusc.get(
                            "sample_data", sample["data"]["LIDAR_TOP"]
                        )["filename"],
                    )
                )
                if exist:
                    self.samples.append(token)
                token = sample["next"]

        self.split = split

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        sample = self.nusc.get("sample", self.samples[idx])
        return LidarNuScenesItem(sample, self.nusc, self.split)


class LidarNuScencesDatamodule(pl.LightningDataModule):
    def __init__(
        self,
        root_dir: str,
        version: str = "v1.0-mini",
        batch_size: int = 32,
        num_workers: int = 1,
    ):
        super().__init__()
        self.root_dir = Path(root_dir)
        self.version = version
        self.batch_size = batch_size
        self.num_workers = num_workers

        self.nus_train = None
        self.nus_val = None
        self.nus_test = None

    def setup(self, stage=None):
        if stage == "fit" or stage is None:
            self.nus_train = LidarNuScenesDataset(
                dataset_version=self.version,
                dataset_root_dir=self.root_dir,
                split="train",
            )
            self.nus_val = LidarNuScenesDataset(
                dataset_version=self.version,
                dataset_root_dir=self.root_dir,
                split="val",
            )

        if stage == "test" or stage is None:
            self.nus_test = LidarNuScenesDataset(
                dataset_version=self.version,
                dataset_root_dir=self.root_dir,
                split="val",
            )

    def train_dataloader(self):
        return DataLoader(
            Subset(self.nus_train, range(10)),
            batch_size=self.batch_size,
            shuffle=True,
            collate_fn=lidar_collate,
            num_workers=self.num_workers,
        )

    def val_dataloader(self):
        return DataLoader(
            self.nus_val,
            batch_size=self.batch_size,
            collate_fn=lidar_collate,
            num_workers=self.num_workers,
        )

    def test_dataloader(self):
        return DataLoader(
            self.nus_test,
            batch_size=self.batch_size,
            collate_fn=lidar_collate,
            num_workers=self.num_workers,
        )
