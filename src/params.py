import math

# bev features parameters
X_RANGE = (-30, 30)
Y_RANGE = (-40, 40)
Z_RANGE = (-3, 3)
CELL_SIZE = 0.1
GRID_SIZE_X = math.ceil((X_RANGE[1] - X_RANGE[0]) / CELL_SIZE)
GRID_SIZE_Y = math.ceil((Y_RANGE[1] - Y_RANGE[0]) / CELL_SIZE)
M = 6

# fov features parameters
HRES = 0.33
VRES = 0.4
VFOV = (-30, 10)
Y_FUDGE = 5
