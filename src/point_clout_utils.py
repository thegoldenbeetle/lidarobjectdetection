import numpy as np

from src import params


def get_density_map(x_indices, y_indices):
    def normalize_density(value):
        return np.minimum(1.0, np.log(value + 1) / np.log(64))

    density_map = np.zeros((params.GRID_SIZE_X, params.GRID_SIZE_Y))
    d_ind, d_value = np.unique(
        np.stack((x_indices, y_indices)), return_counts=True, axis=1
    )
    density_map[d_ind[0], d_ind[1]] = normalize_density(d_value)
    return density_map


def get_intensity_map(pc_points_grid, x_indices, y_indices):
    max_z_index_map = np.full(
        (params.GRID_SIZE_X, params.GRID_SIZE_Y), -1, dtype=int
    )
    for i, (x, y) in enumerate(zip(x_indices, y_indices)):
        cur_max_i = max_z_index_map[x, y]
        if cur_max_i == -1:
            max_z_index_map[x, y] = i
        else:
            max_z_index_map[x, y] = max(
                i, cur_max_i, key=lambda j: pc_points_grid[j, 2]
            )
    intensity_map = np.zeros((params.GRID_SIZE_X, params.GRID_SIZE_Y))
    intensity_map[np.where(max_z_index_map >= 0)] = pc_points_grid[
        max_z_index_map[max_z_index_map >= 0].flatten(), 3
    ]
    return intensity_map


def get_height_map(pc_points_grid, x_indices, y_indices, z_indices):
    height_map = np.full(
        (params.GRID_SIZE_X, params.GRID_SIZE_Y, params.M), -np.inf
    )
    for p, x, y, z in zip(pc_points_grid, x_indices, y_indices, z_indices):
        height_map[x, y, z] = max(height_map[x, y, z], p[2])
    height_map[np.isneginf(height_map)] = params.Z_RANGE[0] - 1
    return height_map


def projection_front_view(  # noqa: PLR0913
    points, v_res, h_res, v_fov, val="depth", y_fudge=0.0
):
    """Takes points in 3D space from LIDAR data and projects them to a 2D
        "front view" image, and saves that image.

    Args:
    ----
        points: (np array)
            The numpy array containing the lidar points.
            The shape should be Nx4
            - Where N is the number of points, and
            - each point is specified by 4 values (x, y, z, reflectance)
        v_res: (float)
            vertical resolution of the lidar sensor used.
        h_res: (float)
            horizontal resolution of the lidar sensor used.
        v_fov: (tuple of two floats)
            (minimum_negative_angle, max_positive_angle)
        val: (str)
            What value to use to encode the points that get plotted.
            One of {"depth", "height", "reflectance"}
        y_fudge: (float)
            A hacky fudge factor to use if the theoretical calculations of
            vertical range do not match the actual data.
            For a Velodyne HDL 64E, set this value to 5.
    """
    # pylint: disable=R0914

    # validating the inputs
    assert (
        len(v_fov) == 2  # noqa: PLR2004
    ), "v_fov must be list/tuple of length 2"
    assert v_fov[0] <= 0, "first element in v_fov must be 0 or negative"
    assert val in {
        "depth",
        "height",
        "reflectance",
    }, 'val must be one of {"depth", "height", "reflectance"}'

    x_lidar = points[:, 0]
    y_lidar = points[:, 1]
    z_lidar = points[:, 2]
    r_lidar = points[:, 3]  # Reflectance
    # Distance relative to origin when looked from top
    d_lidar = np.sqrt(x_lidar**2 + y_lidar**2)
    # Absolute distance relative to origin
    # d_lidar = np.sqrt(x_lidar ** 2 + y_lidar ** 2, z_lidar ** 2)

    v_fov_total = -v_fov[0] + v_fov[1]

    # Convert to Radians
    v_res_rad = v_res * (np.pi / 180)
    h_res_rad = h_res * (np.pi / 180)

    # PROJECT INTO IMAGE COORDINATES
    x_img = np.arctan2(-y_lidar, x_lidar) / h_res_rad
    y_img = np.arctan2(z_lidar, d_lidar) / v_res_rad

    # SHIFT COORDINATES TO MAKE 0,0 THE MINIMUM
    x_min = -360.0 / h_res / 2  # Theoretical min x value based on sensor specs
    x_img -= x_min  # Shift
    x_max = 360.0 / h_res  # Theoretical max x value after shifting

    y_min = v_fov[0] / v_res  # theoretical min y value based on sensor specs
    y_img -= y_min  # Shift
    y_max = v_fov_total / v_res  # Theoretical max x value after shifting

    y_max += y_fudge  # Fudge factor if the calculations based on
    # spec sheet do not match the range of
    # angles collected by in the data.

    # WHAT DATA TO USE TO ENCODE THE VALUE FOR EACH PIXEL
    if val == "reflectance":
        pixel_values = r_lidar
    elif val == "height":
        pixel_values = z_lidar
    else:
        pixel_values = -d_lidar

    y_max = int(y_max) + 1
    x_max = int(x_max) + 1
    y_img = y_max - y_img
    fov_map = np.zeros((y_max, x_max))
    mask = (
        (x_img >= 0)
        & (y_img >= 0)
        & (x_img < fov_map.shape[1])
        & (y_img < fov_map.shape[0])
    )
    x_img = x_img[mask]
    y_img = y_img[mask]
    pixel_values = pixel_values[mask]

    fov_map[y_img.astype(int), x_img.astype(int)] = pixel_values

    return fov_map
