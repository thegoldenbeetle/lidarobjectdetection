nuscenes_category_to_our = {
    "animal": "animal",
    "human.pedestrian.adult": "human",
    "human.pedestrian.child": "human",
    "human.pedestrian.construction_worker": "human",
    "human.pedestrian.personal_mobility": "human",
    "human.pedestrian.police_officer": "human",
    "human.pedestrian.stroller": "human",
    "human.pedestrian.wheelchair": "human",
    "movable_object.barrier": "other",
    "movable_object.debris": "other",
    "movable_object.pushable_pullable": "other",
    "movable_object.trafficcone": "other",
    "static_object.bicycle_rack": "other",
    "vehicle.bicycle": "vehicle",
    "vehicle.bus.bendy": "vehicle",
    "vehicle.bus.rigid": "vehicle",
    "vehicle.car": "vehicle",
    "vehicle.construction": "vehicle",
    "vehicle.emergency.ambulance": "vehicle",
    "vehicle.emergency.police": "vehicle",
    "vehicle.motorcycle": "vehicle",
    "vehicle.trailer": "vehicle",
    "vehicle.truck": "vehicle",
}

category_to_id = {"animal": 0, "human": 1, "vehicle": 2, "other": 3}

category_id_to_color = {0: "green", 1: "red", 2: "blue", 3: "grey"}
