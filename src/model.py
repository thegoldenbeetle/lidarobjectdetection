import torch
from torch import nn


class CustomResNet(nn.Module):
    def __init__(self, input_channels):
        super().__init__()
        resnet = torch.hub.load(
            "pytorch/vision:v0.10.0", "resnet34", pretrained=True
        )
        resnet.conv1 = nn.Conv2d(
            input_channels,
            64,
            kernel_size=(7, 7),
            stride=(2, 2),
            padding=(3, 3),
            bias=False,
        )
        self.resnet_backbone = nn.Sequential(
            resnet.conv1,
            resnet.bn1,
            resnet.relu,
            resnet.maxpool,
            resnet.layer1,
            resnet.layer2,
        )

    def forward(self, x):
        x = self.resnet_backbone(x)
        return x


class MV3DModel(torch.nn.Module):
    def __init__(self, bev_channels=8):
        super().__init__()
        self.bev_backbone = CustomResNet(bev_channels)
        self.upsample = nn.ConvTranspose2d(128, 16, 3, stride=2, padding=1)

        # self.linear1 = torch.nn.Linear(100, 200)
        # self.activation = torch.nn.ReLU()
        # self.linear2 = torch.nn.Linear(200, 10)
        # self.softmax = torch.nn.Softmax()

    def forward(self, x):
        x = self.bev_backbone(x)
        x = self.upsample(x)
        # x = self.linear1(x)
        # x = self.activation(x)
        # x = self.linear2(x)
        # x = self.softmax(x)
        return x
