from typing import Sequence, Tuple, Union

import lightning.pytorch as pl
import torch
from torch import nn, optim
from torch.optim.lr_scheduler import OneCycleLR

from src import params


class CustomOneCycleLR(OneCycleLR):
    def __init__(self, *args, second_strategy: str = "cos", **kwargs):
        self.second_strategy = second_strategy
        super().__init__(*args, **kwargs)

    def step(self, epoch=None):
        if self.last_epoch > self._schedule_phases[0]["end_step"]:
            if self.second_strategy not in ["cos", "linear"]:
                raise ValueError(
                    "anneal_strategy must by one of 'cos' "
                    f"or 'linear', instead got {self.second_strategy}"
                )
            if self.second_strategy == "cos":
                self.anneal_func = self._annealing_cos
            elif self.second_strategy == "linear":
                self.anneal_func = self._annealing_linear
        super().step(epoch=epoch)


class ResUnit(nn.Module):
    def __init__(
        self,
        input_channels: int = 32,
        output_channels: int = 32,
        first_stride: Union[int, Tuple[int, int]] = 1,
    ):
        super().__init__()

        self.components = nn.Sequential(
            nn.BatchNorm2d(input_channels),
            nn.ReLU(),
            nn.Conv2d(
                input_channels,
                output_channels,
                kernel_size=3,
                stride=first_stride,
                padding=1,
            ),
            nn.BatchNorm2d(output_channels),
            nn.ReLU(),
            nn.Conv2d(
                output_channels,
                output_channels,
                kernel_size=3,
                stride=1,
                padding=1,
            ),
        )
        self.res_conv = nn.Conv2d(
            input_channels,
            output_channels,
            kernel_size=3,
            stride=first_stride,
            padding=1,
        )

    def forward(self, x):
        return self.components(x) + self.res_conv(x)


class ResBlock(nn.Module):
    def __init__(
        self,
        blocks: Sequence[int],
        input_channels: int = 32,
    ):
        super().__init__()

        units = []
        blocks = [input_channels, *blocks]
        for i, (prev_bsize, bsize) in enumerate(zip(blocks[:-1], blocks[1:])):
            units.append(
                ResUnit(prev_bsize, bsize, first_stride=1 if i else 2)
            )
        self.units = nn.Sequential(*units)

    def forward(self, x):
        return self.units(x)


class UpsampelBlock(nn.Module):
    def __init__(
        self,
        input_channels: int = 32,
        skip_con_channels: int = 32,
        output_channels: int = 32,
    ):
        super().__init__()

        self.deconv = nn.ConvTranspose2d(
            input_channels, output_channels, kernel_size=3, stride=2, padding=1
        )
        self.skip_conv = nn.Conv2d(
            skip_con_channels, output_channels, kernel_size=1
        )

    def forward(self, x, skip_x):
        skip_x = self.skip_conv(skip_x)
        out_padding = [1, 1]
        if abs(skip_x.shape[2] - x.shape[2] * 2) > 0:
            out_padding[0] = 0
        if abs(skip_x.shape[3] - x.shape[3] * 2) > 0:
            out_padding[1] = 0
        self.deconv.output_padding = out_padding
        x = self.deconv(x)
        return x + skip_x


class PIXORHeader(nn.Module):
    def __init__(self, input_channels: int = 32, length: int = 4):
        super().__init__()

        self.header = nn.Sequential(
            *[
                nn.Sequential(
                    nn.Conv2d(
                        input_channels,
                        input_channels,
                        kernel_size=3,
                        stride=1,
                        padding=1,
                    ),
                    nn.BatchNorm2d(input_channels),
                    nn.ReLU(),
                )
                for _ in range(length)
            ]
        )
        self.class_conv = nn.Sequential(
            nn.Conv2d(input_channels, 1, kernel_size=3, stride=1, padding=1),
            nn.Sigmoid(),
        )
        self.box_conv = nn.Conv2d(
            input_channels, 7, kernel_size=3, stride=1, padding=1
        )

    def forward(self, x):
        x = self.header(x)
        return (self.class_conv(x), self.box_conv(x))


class PIXORBackbon(nn.Module):  # pylint: disable=R0902
    def __init__(self, input_channels: int = 64):
        super().__init__()

        self.block1 = nn.Sequential(
            nn.Conv2d(input_channels, 32, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.Conv2d(32, 32, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
        )
        self.block2 = ResBlock([24] * 2 + [96], 32)
        self.block3 = ResBlock([48] * 5 + [192], 96)
        self.block4 = ResBlock([64] * 5 + [256], 192)
        self.block5 = ResBlock([96] * 2 + [384], 256)
        self.conv_channels = nn.Conv2d(384, 196, kernel_size=1)
        self.block6 = UpsampelBlock(196, 256, 128)
        self.block7 = UpsampelBlock(128, 192, 96)

    def forward(self, x):
        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        block3_out = x
        x = self.block4(x)
        block4_out = x
        x = self.block5(x)
        x = self.conv_channels(x)
        x = self.block6(x, block4_out)
        x = self.block7(x, block3_out)
        return x


class PIXOR(pl.LightningModule):
    def __init__(self, input_channels: int = 64):
        super().__init__()

        self.backbone = PIXORBackbon(input_channels=input_channels)
        self.header = PIXORHeader(input_channels=96)

    def forward(self, x):  # pylint: disable=W0221
        x = self.backbone(x)
        x = self.header(x)
        return x

    def _build_targets(
        self, batch_annotations, cls_shape, box_reg_shape
    ):  # pylint: disable=R0914
        cls_target = torch.zeros(cls_shape).to(self.device)
        box_reg_target = torch.zeros(box_reg_shape).to(self.device)
        for i, annotations in enumerate(batch_annotations):
            for x, y, _, w, l, h, yaw, _, gx, gy, _ in annotations:
                with torch.no_grad():
                    grid_x, grid_y = (
                        int(gx * cls_shape[0]),
                        int(gy * cls_shape[1]),
                    )

                    cls_target[i, :, grid_x, grid_y] = 1

                    cell_x, cell_y = (
                        int(gx * params.GRID_SIZE_X) * params.CELL_SIZE
                        + params.X_RANGE[0],
                        int(gy * params.GRID_SIZE_Y) * params.CELL_SIZE
                        + params.Y_RANGE[0],
                    )
                    box_reg_target[i, :, grid_x, grid_y] = torch.Tensor(
                        [
                            torch.cos(yaw),
                            torch.sin(yaw),
                            torch.log(torch.abs(x - cell_x + 1e-8)),
                            torch.log(torch.abs(y - cell_y + 1e-8)),
                            torch.log(w),
                            torch.log(l),
                            torch.log(h),
                        ]
                    ).to(self.device)
        return cls_target, box_reg_target

    def training_step(self, batch, batch_idx):  # pylint: disable=W0221
        del batch_idx
        _, bev, _, batch_annotations = batch
        batch_size = bev.shape[0]
        cls, box_reg = self(bev)

        cls_target, box_reg_target = self._build_targets(
            batch_annotations, cls.shape, box_reg.shape
        )
        loss_reg = (
            nn.functional.smooth_l1_loss(
                box_reg * (cls_target > 0), box_reg_target, reduction="sum"
            )
            / sum((len(ann) for ann in batch_annotations))
            / box_reg_target.shape[1]
        )
        loss_cls = nn.functional.binary_cross_entropy(
            cls, cls_target, reduction="mean"
        )
        loss = loss_cls + loss_reg

        self.log("train/loss", loss, prog_bar=True, batch_size=batch_size)
        self.log(
            "train/loss_reg", loss_reg, prog_bar=True, batch_size=batch_size
        )
        self.log(
            "train/loss_cls", loss_cls, prog_bar=True, batch_size=batch_size
        )
        return loss

    def validation_step(self, batch, batch_idx):  # pylint: disable=W0221,W0613
        # del batch_idx
        _, bev, _, batch_annotations = batch
        batch_size = bev.shape[0]

        if torch.sum(torch.isfinite(bev)) > 0:
            self.log("val/loss", 10.0, prog_bar=True, batch_size=batch_size)
            self.log(
                "val/loss_reg", 10.0, prog_bar=True, batch_size=batch_size
            )
            self.log(
                "val/loss_cls", 10.0, prog_bar=True, batch_size=batch_size
            )
            return 10.0
        cls, box_reg = self(bev)

        cls_target, box_reg_target = self._build_targets(
            batch_annotations, cls.shape, box_reg.shape
        )
        loss_reg = nn.functional.smooth_l1_loss(
            box_reg * (cls_target > 0), box_reg_target, reduction="sum"
        ) / sum((len(ann) for ann in batch_annotations))
        loss_cls = nn.functional.binary_cross_entropy(
            cls, cls_target, reduction="mean"
        )
        loss = loss_cls + loss_reg

        self.log("val/loss", loss, prog_bar=True, batch_size=batch_size)
        self.log(
            "val/loss_reg", loss_reg, prog_bar=True, batch_size=batch_size
        )
        self.log(
            "val/loss_cls", loss_cls, prog_bar=True, batch_size=batch_size
        )
        return loss

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=1e-3)
        scheduler = {
            "scheduler": CustomOneCycleLR(
                optimizer,
                max_lr=1e-3,
                epochs=self.trainer.max_epochs,
                steps_per_epoch=self.trainer.num_train_batches,
                pct_start=0.2,
                div_factor=20,
                anneal_strategy="linear",
            ),
            "interval": "step",
            "frequency": 1,
            "name": "learning_rate",
        }
        return optimizer, scheduler


# if __name__ == "__main__":
#     m = PIXOR(36)
#     m.train()
#     x = torch.rand(2, 36, 800, 700)
#     gx, gy, gz = (
#         np.array((2, -39.05, 0.5))
#         - [params.X_RANGE[0], params.Y_RANGE[0], params.Z_RANGE[0]]
#     ) / [
#         (params.X_RANGE[1] - params.X_RANGE[0]),
#         (params.Y_RANGE[1] - params.Y_RANGE[0]),
#         (params.Z_RANGE[1] - params.Z_RANGE[0]),
#     ]
#     annotations = [
#         torch.Tensor([(2, -39.05, 0.5, 1.0, 1.0, 1.0, 0.23, 1, gx, gy, gz)]),
#         torch.Tensor([(2, -39.05, 0.5, 1.0, 1.0, 1.0, 0.23, 1, gx, gy, gz)]),
#     ]
#     # print(m(x)[0].shape, m(x)[1].shape)

#     opt = m.configure_optimizers()
#     loss = m.training_step([None, x, None, annotations], 0)
#     loss.backward()
