
lint: lint/black lint/isort lint/pylint  

lint/isort:
	isort src

lint/black:
	black .

lint/pylint:
	pylint src

