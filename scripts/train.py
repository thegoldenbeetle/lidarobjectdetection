#!/usr/bin/env python3
import lightning.pytorch as pl
from pytorch_lightning.callbacks import LearningRateMonitor

from src.dataset import LidarNuScencesDatamodule
from src.pixor import PIXOR

if __name__ == "__main__":
    callbacks = [LearningRateMonitor(logging_interval="step")]
    model = PIXOR(8, callbacks=callbacks)

    datamodule = LidarNuScencesDatamodule(
        "/data/Datasets/nuScenes",
        version="v1.0-trainval",
        batch_size=4,
        num_workers=8,
    )
    trainer = pl.Trainer(log_every_n_steps=5)
    trainer.fit(model, datamodule)
